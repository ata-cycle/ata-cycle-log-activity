<?php

namespace Ata\Cycle\LogActivity\Models\Traits;

use Cycle\Annotated\Annotation\Relation\Morphed\MorphedHasMany;

use Ata\Cycle\LogActivity\Models\LogActivity;

trait Loggable
{
    /** @MorphedHasMany(target = LogActivity::class, outerKey="loggable_id", morphKey="loggable_type") */
    public $logs;
}
