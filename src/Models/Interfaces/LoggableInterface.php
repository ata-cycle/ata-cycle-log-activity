<?php


namespace Ata\Cycle\LogActivity\Models\Interfaces;

use Ata\Cycle\ORM\Models\Interfaces\CycleModelInterface;

/**
 * Empty interface, need only for check, what to log
*/
interface LoggableInterface extends CycleModelInterface
{

}
