<?php

namespace Ata\Cycle\LogActivity\Models;

use Ata\Cycle\ORM\Models\CycleModel;
use Ata\Cycle\ORM\Models\Traits\IntPrimary;
use Ata\Cycle\ORM\Models\Traits\Timestamps;
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\Morphed\BelongsToMorphed;
use Cycle\Annotated\Annotation\Table;
use Cycle\Annotated\Annotation\Table\Index;
use Ata\Cycle\ORM\Typecasts\Json;
use Ata\Cycle\LogActivity\Models\Interfaces\LoggableInterface;
use Ata\Cycle\ORM\Constrains\EmptyConstrain;

/**
 * @Entity(constrain=EmptyConstrain::class)
 *
 * @Table(indexes = {
 *          @Index(columns={"log_name"}),
 *          @Index(columns={"causer_id", "causer_type"}, name="causer"),
 *     })
 */
class LogActivity extends CycleModel
{
    use IntPrimary;
    use Timestamps;

    /** @Column(type="string", nullable=true) */
    public $log_name;

    /** @Column(type="text", nullable=true) */
    public $description;

    /** @Column(type="bigInteger", nullable=true) */
    public $causer_id;

    /** @Column(type="string", nullable=true) */
    public $causer_type;

    public $loggable_id;
    public $loggable_type;

    /** @Column(type="json", nullable=true, typecast=Json::class) */
    public $properties;

    /**
     * @BelongsToMorphed(target=LoggableInterface::class,
     *     nullable=true,
     *     innerKey="loggable_id",
     *     morphKey="loggable_type"
     * )
    */
    private $loggable;

    public static function logEntityChange($entity, array $changedData, string $action)
    {
        static::logEntityChangeForCommands($entity, $changedData, $action);
        $entity->save();
    }

    public static function logEntityChangeForCommands($entity, array $changedData, string $action)
    {
        $user = auth()->user();

        $data = [
            'log_name' => 'models_tracking',
            'description' => $action,
            'properties' => $changedData
        ];

        if ($user !== null){
            $data += [
                'causer_id' => $user->id,
                'causer_type' => get_class($user),
            ];
        }

        $entity->logs->add(LogActivity::make($data));
    }

    public function loggable(){
        return $this->loggable->__resolve();
    }
}
