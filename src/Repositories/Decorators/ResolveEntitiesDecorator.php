<?php

namespace Ata\Cycle\LogActivity\Repositories\Decorators;

use Cycle\ORM\Schema;
use Spiral\Database\Injection\Parameter;

class ResolveEntitiesDecorator
{
    public static function resolveEntities($result)
    {
        $schema = resolve('cycle-db')->getSchema();

        $result
            ->map(function ($elem) {
                return ['id' => $elem->loggable_id, 'role' => $elem->loggable_type];
            })
            ->groupBy('role')
            ->each(function ($elems, $role) use ($schema) {
                $entityClass = $schema->define($role, Schema::ENTITY);

                $ids = $elems->map(function ($elem) {
                    return $elem['id'];
                })->unique()->all();
                $entityClass::where('id', 'in', new Parameter(
                    $ids
                ))->findAll();
            });
        return $result;
    }
}
