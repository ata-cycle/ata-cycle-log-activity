<?php

namespace Ata\Cycle\LogActivity\MapperCommands\Traits;

trait CompareTrait
{
    public static function compare($a, $b): int
    {
        if ($a == $b) {
            return 0;
        }

        return ($a > $b) ? 1 : -1;
    }
}
