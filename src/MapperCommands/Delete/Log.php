<?php

namespace Ata\Cycle\LogActivity\MapperCommands\Delete;

use Ata\Cycle\LogActivity\Models\Interfaces\LoggableInterface;
use Ata\Cycle\ORM\Mappers\Commands\Interfaces\DeleteMapperCommand;
use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Ata\Cycle\LogActivity\MapperCommands\Traits\CompareTrait;
use Ata\Cycle\LogActivity\Models\LogActivity;
use Cycle\ORM\Command\CommandInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;

class Log  implements DeleteMapperCommand
{
    use CompareTrait;

    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?CommandInterface $previousCommand): ?CommandInterface
    {
        if ($entity instanceof LoggableInterface) {
            $data = $mapper->fetchEntityFields($entity);
            LogActivity::logEntityChangeForCommands(
                $entity,
                array_udiff_assoc($data, $state->getData(), [static::class, 'compare']),
                config('cycle-logging.events.deleted')
            );
            $entity->save();
        }

        return $previousCommand;
    }
}
