<?php

namespace Ata\Cycle\LogActivity\MapperCommands\Update;

use Ata\Cycle\LogActivity\Models\Interfaces\LoggableInterface;
use Ata\Cycle\ORM\Mappers\Commands\Interfaces\UpdateMapperCommand;
use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Ata\Cycle\LogActivity\MapperCommands\Traits\CompareTrait;
use Ata\Cycle\LogActivity\Models\LogActivity;
use Cycle\ORM\Command\ContextCarrierInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;

class Log implements UpdateMapperCommand
{
    use CompareTrait;

    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?ContextCarrierInterface $previousCommand): ?ContextCarrierInterface
    {
        if ($entity instanceof LoggableInterface) {
            $data = $mapper->fetchEntityFields($entity);

            LogActivity::logEntityChangeForCommands(
                $entity,
                array_udiff_assoc($data, $state->getData(), [static::class, 'compare']),
                config('cycle-logging.events.updated')
            );
        }

        return $previousCommand;
    }
}
