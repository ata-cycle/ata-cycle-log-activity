<?php

namespace Ata\Cycle\LogActivity\MapperCommands\Create;

use Ata\Cycle\LogActivity\Models\Interfaces\LoggableInterface;
use Ata\Cycle\ORM\Mappers\Commands\Interfaces\CreateMapperCommand;
use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Ata\Cycle\LogActivity\Models\LogActivity;
use Cycle\ORM\Command\ContextCarrierInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;

class Log implements CreateMapperCommand
{
    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?ContextCarrierInterface $previousCommand): ?ContextCarrierInterface
    {
        if ($entity instanceof LoggableInterface) {
            LogActivity::logEntityChangeForCommands(
                $entity,
                $state->getData(),
                config('cycle-logging.events.created')
            );
        }

        return $previousCommand;
    }
}
