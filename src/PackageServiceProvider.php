<?php

namespace Ata\Cycle\LogActivity;

use Illuminate\Support\ServiceProvider;

class PackageServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/cycle-logging.php' => config_path('cycle-logging.php')
        ]);

        $this->mergeConfigFrom(
            __DIR__ . '/../config/cycle-logging.php', 'cycle-logging'
        );
    }
}
