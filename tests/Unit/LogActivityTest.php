<?php

namespace Ata\Cycle\LogActivity\Tests\Unit;

use Ata\Cycle\LogActivity\Models\LogActivity;
use Ata\Cycle\LogActivity\Tests\BaseTestCase;
use Ata\Cycle\LogActivity\Tests\Models\TestModel;
use Cycle\ORM\Promise\PromiseInterface;

class LogActivityTest extends BaseTestCase
{
    public function testShouldSaveEntityAndReturnId()
    {
        $model = TestModel::create(['integer_field'=>1]);

        self::assertNotNull($model->id);
    }

    public function testShouldCreateLogActivity()
    {
        TestModel::create(['integer_field'=>1]);

        self::assertEquals(1, LogActivity::count());
    }

    public function testShouldQueryOnLogActivityThroughParentEntity()
    {
        $model = TestModel::create(['integer_field'=>1]);
        $model->update(['integer_field'=>2]);
        $model->delete();

        TestModel::create(['integer_field'=>1]);

        resolve('cycle-db.heap-clean');

        $actualCount = TestModel::where('logs.description', config('cycle-logging.events.deleted'))
            ->withoutConstrains()
            ->count();

        self::assertEquals(1, $actualCount);
    }

    public function testShouldReturnParentEntityViaLoad()
    {
        $model = TestModel::create(['integer_field'=>1]);
        $model->update(['integer_field'=>2]);
        $model->delete();

        TestModel::create(['integer_field'=>1]);

        resolve('cycle-db.heap-clean');

        $logActivity = LogActivity::firstOrFail();

        $this->assertInstanceOf(PromiseInterface::class, $logActivity->loggable);

        dump($logActivity->loggable->__resolve());
    }

    protected function dropTables(): bool
    {
     return false;
    }

    public function testShouldQueryOnParentEntityThroughLogActivity()
    {
        $model = TestModel::create(['integer_field'=>1]);
        $model->update(['integer_field'=>2]);
        $model->delete();

        TestModel::create(['integer_field'=>1]);

        resolve('cycle-db.heap-clean');

        $actualCount = LogActivity::where('loggable.integer_field', 1)->withoutConstrain()->count();
        self::assertEquals(1, $actualCount);
    }
}
