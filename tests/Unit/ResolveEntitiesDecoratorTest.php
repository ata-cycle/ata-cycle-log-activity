<?php

namespace Ata\Cycle\LogActivity\Tests\Unit;

use Ata\Cycle\LogActivity\Models\LogActivity;
use Ata\Cycle\LogActivity\Repositories\Decorators\ResolveEntitiesDecorator;
use Ata\Cycle\LogActivity\Tests\BaseTestCase;
use Ata\Cycle\LogActivity\Tests\Models\TestModel;

class ResolveEntitiesDecoratorTest extends BaseTestCase
{
    protected function createEntities()
    {
        $model = TestModel::create(['integer_field'=>1]);
        $model->update(['integer_field'=>134]);
        $model->delete();

        TestModel::create(['integer_field'=>1]);
        TestModel::create(['integer_field'=>2]);
        TestModel::create(['integer_field'=>3]);
        TestModel::create(['integer_field'=>4]);
        TestModel::create(['integer_field'=>5]);
    }

    public function testShouldResolveAllEntities()
    {
        $logActivity = ResolveEntitiesDecorator::resolveEntities(LogActivity::findAll());

        $logActivity->each(function($elem){
            self::assertNotNull($elem->loggable()->integer_field);
        });
    }
}
