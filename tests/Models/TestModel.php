<?php

namespace Ata\Cycle\LogActivity\Tests\Models;

use Ata\Cycle\LogActivity\Models\Interfaces\LoggableInterface;
use Ata\Cycle\LogActivity\Models\Traits\Loggable;
use Ata\Cycle\ORM\Models\Traits\IntPrimary;
use Ata\Cycle\ORM\Models\Traits\SoftDeletes;
use Ata\Cycle\ORM\Models\Traits\Timestamps;
use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;

/**
 * @Entity
 *
*/
class TestModel extends BaseTestModel implements LoggableInterface
{
    use IntPrimary;
    use Timestamps;
    use SoftDeletes;
    use Loggable;

    /** @Column(type="integer", nullable=true) */
    public $integer_field;
}
