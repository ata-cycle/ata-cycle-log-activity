<?php

namespace Ata\Cycle\LogActivity\Tests;

use Ata\Cycle\LogActivity\MapperCommands\Create\Log as CreateLog;
use Ata\Cycle\LogActivity\MapperCommands\Delete\Log as DeleteLog;
use Ata\Cycle\LogActivity\MapperCommands\Update\Log as UpdateLog;
use Ata\Cycle\LogActivity\Tests\Models\TestModel;
use Ata\Cycle\ORM\Mappers\Commands\Create\Create;
use Ata\Cycle\ORM\Mappers\Commands\Update\Update;
use Ata\Cycle\ORM\Testing\BaseDBTestCase;

abstract class BaseTestCase extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);

        $loggingConfig = include __DIR__ . '/../config/cycle-logging.php';

        $app['config']->set('cycle-logging', $loggingConfig);

        $app['config']->set('cycle.schema.path', ['/app/tests/Models', '/app/src/Models']);

        $app['config']->set('cycle.commands.create', [
            new Create(),
            new \Ata\Cycle\ORM\Mappers\Commands\Create\Timestamps(),
            new CreateLog(),
        ]);

        $app['config']->set('cycle.commands.update', [
            new Update(),
            new \Ata\Cycle\ORM\Mappers\Commands\Update\Timestamps(),
            new UpdateLog(),
        ]);

        $app['config']->set('cycle.commands.delete', [
            new \Ata\Cycle\ORM\Mappers\Commands\Delete\SoftDelete(),
            new DeleteLog(),
        ]);
    }

    protected function getPackageProviders($app)
    {
        return array_merge(
            parent::getPackageProviders($app),
            [
                'Ata\Cycle\LogActivity\PackageServiceProvider'
            ]
        );
    }
}
