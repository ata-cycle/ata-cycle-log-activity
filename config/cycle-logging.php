<?php

return [
    'events' => [
        'created' => 'created',
        'updated' => 'updated',
        'deleted' => 'deleted',
    ]
];